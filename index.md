# tutoriel wiki. erg. be

<span> Le site de l'erg est un wiki, un espace collaboratif où tout..e utilisateur..ice peut créer une page et documenter l'évolution de la vie dans l'établissement.
Ce qui veut dire que nous, étudiant.es, pouvons nous en emparer pour partager un maximum de savoirs et de projets, d'initiatives, et même de travaux personnels!
<br><br>Empare-toi de cet instrument! Partage ton collectif, parle d'un atelier collaboratif, ou montre le suivi de ton projet à tes profs.
Pour que le site de l'erg ne soit pas un simple présentoir, mais un véritable espace pour toustes!
</span>

## Général

⚠️ ***Veillez à être respectueux..ses dans le langage et à ne pas
déranger les contributions des autres utilis️ateur..ice..s***


Ce site est basé sur la participation, l'idée générale comprend que le
site soit rempli de contenu provenant des élèves et/ou
profs/représentant..e..s administratif..v..es.<br>
À priori, rien n'est interdit, vous avez la liberté d'écrire ce que
vous voulez, d'uploader ce que vous voulez... Ceci dit, il s'agit de
votre responsabilité.

### Compte 

Avant de commencer à modifier le site, il faut te créer un compte. 
Tu pourras alors créer
une page d'utilisateur..ice qui documente tes contributions ou qui te
présente, toi et ton travail, tes projets. Insére-y tes liens vers
d'autres sites ou contacts, utilise-la comme une page personnelle dans
l'espace numérique de l'école!

Tourne la page pour un tutoriel pour contribuer au wiki si tu n'arrives pas à t'y situer.

## Tutoriel

### Créer un compte  

Crée-toi un compte en cliquant sur l'icône d'utilisateur en haut à
gauche. Clique sur 'Se connecter'. Ensuite en bas de la page, tu
verras un bouton 'Rejoignez erg'. Introduis un nom d'utilisateur au
choix, un mot de passe, ton courriel. Tu peux aussi introduire ton 'nom
réel', le nom qui s'affichera sur les historiques de modifications et
de contributions.

### Modifier sa page d'utilisateur  

Une fois connecté..e, si tu cliques sur l'icône d'utilisateur·ice puis
sur ton pseudo, tu atterriras sur Utilisateur:*\[tonpseudo\]*. C'est
une page qui t'es dédiée. Remplis-la avec des infos sur toi, des liens
vers tes autres sites ou profils, présente le suivi de tes projets\...
Utilise cette page comme un mini-site.

### Modifier l'apparence du wiki  

Plusieurs thèmes sont disponibles dans les
préférences utilisateur..ice. Ouroboros est pour l'instant par défaut et est le plus travaillé. Si tu es à l'aise avec le CSS et veux contribuer, n'hésite pas à modifier les feuilles de style (lien dans l'overture du livret).

### Créer une page  

Il n'y a pas à proprement parler de bouton pour créer une nouvelle
page. Pour en créer une, tu peux :

-   Écrire un lien vers le titre de la nouvelle page souhaitée, dans le
    contenu d'une autre page qui y serait liée: `[[Nom_de_la_page]]`
    dans le contenu d'une autre page déjà existante. Sauvegarde les
    changements. Clique sur le lien que tu viens d'écrire pour créer la
    nouvelle page.
-   Rechercher le nom souhaité dans la barre de recherche en haut ☝️. Si
    la page n'existe pas, il te sera proposé de la créer.

Tu arriveras devant un champs de texte qui te proposera d'introduire le
contenu de la page. Rédige le contenu et appuie sur 'Enregistrer les
modifications'

#### Insérer un fichier  

Pour insérer un fichier sur la page, clique sur l'icône d'une image,
qui te permettra de soit relier un nom d'image déjà téléversée, soit
d'en téléverser une. Choisis l'option qui te convient. Cela devrait
créer dans ton texte un lien qui ressemble à
`[[Fichier:Mon_fichier_importé.png]]`.

🥋 ***Tu es désormais un·e wikinaute ceinture noire douzième dan.*** 🥋

### Pour aller plus loin  

À l'issue du Workshop : WIKI
MAINTENANCE (winterschool
2024), une problématique récurrente s'est présentée. Le site nous
appartient, nous élèves, alors, comment pourrions-nous, en tant
qu'utilisateur..ices, faire pour que ce site devienne une surface
artistique en tant que telle, un espace libre de production de contenu
quelqu'il soit ?

#### Syntaxe pour écrire des articles wiki:<br>
mediawiki.org/wiki/Help:Formatting/fr
#### Ce guide et plus en ligne:<br>
wiki.erg.be/w/Guide_Wiki
#### Pages spéciales du wiki:<br>
wiki.erg.be/w/Spécial:Pages_spéciales
#### Si tu connais CSS et vois un problème de thème:<br>
wiki.erg.be/w/MediaWiki:Ouroboros.css

#### wiki.erg.be/m/
pour la version avec la carte des pages

#### wiki.erg.be/w/
pour une version avec le wiki seulement

*Déplie vers le haut pour lire les conseils de contribution*

### Conseils

1. ***Création de page*** vérifie si la page existe déjà en tapant le titre dans la barre de recherche - créer deux pages identiques fait un problème dans le wiki.
<br>
2. Evite de trop modifier une page existante sans l'accord de la personne ou le groupe de personnes que cette dernière représente.
<br>
3. Si tu finis par ne pas aimer les modifications que tu as apporté, clique sur *Actions*>*Historique* et sélectionne la version de la page avant ton enregistrement. Toute modification est identifiée et datée.
<br>
4. ***Page personnelle***: remplis-la d'une description, de tes travaux (avec ou sans photos) et crée une section de suivi de projets, à envoyer à tes profs au cas où, si tu veux. C'est un espace disponible pour te représenter librement, saisis donc l'occasion.
<br>
5. ***Titres***: Choisis un titre qui sera facile d'accès pour une page de collectif..ve ou de projet, facile à rechercher et sans trop de caractères spéciaux. Celà permettra a ceux qui explorent le site de la trouver plus facilement.
<br>
6. ***Liens***: Pour contribuer à une expérience meilleure du site, insère des liens vers des pages là où il serait intéressant de les mettre. Ceci permet d'avoir moins de pages "orphelines" qui sont déconnéctées et donc parfois introuvables.
