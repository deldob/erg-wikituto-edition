$.get('index.md', function(data) {
		    var converter = new showdown.Converter(),
				        html = converter.makeHtml(data);
		    $('#book-content').html(html);
}).fail(function() {
		    console.error('Failed to load markdown file');
});
